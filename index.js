const csv = require("csvtojson");
const lodash = require("lodash");


const MATCHES_FILE_PATH = "./csv_data/matches.csv";
const DELIVERIES_FILE_PATH = "./csv_data/deliveries.csv";





const getStrikeRate = (matches,deliveries)=>{
    const batsman = deliveries.filter(eachBall => eachBall.batsman === "MS Dhoni");

    const yearWiseDeliveries = batsman.map(eachBall=>{
        let year = matches.filter(match=>match.id === eachBall.match_id).map(eachYear=>eachYear.season);
        eachBall.season = year[0]
        return eachBall
    });

    const yearWiseStrikeRate = yearWiseDeliveries.reduce((acc,eachBall)=>{
            if(acc[eachBall.season]){
                acc[eachBall.season]["runs"] += Number(eachBall.batsman_runs);
                acc[eachBall.season]["balls"]++;
                acc[eachBall.season]["strikeRate"] = acc[eachBall.season].runs/acc[eachBall.season].balls*100
            }else{
                acc[eachBall.season] = {};
                acc[eachBall.season]["runs"] = Number(eachBall.batsman_runs);
                acc[eachBall.season]["balls"] = 1;
            }

            return acc;
    },{});

    console.log(yearWiseStrikeRate);

}



//............................................................................//


const getHighestDismissal = (deliveries)=>{
    const bowlers = deliveries.filter(eachBall=> eachBall.player_dismissed === "MS Dhoni" && eachBall.dismissal_kind !== "run out").map(eachBall=> eachBall.bowler);
    
    const result = bowlers.reduce((acc,curr)=>{
            acc[curr] = acc[curr] ? acc[curr]+=1 : 1 ;
            return acc;
    },{})

    const res = Object.entries(result).sort(([,a],[,b])=>[,b][1]-[,a][1])[0];
    
    // const temp = new Map();
    console.log(res);

}


//...........................................................................//

const getBestEcoInSuperOver = (deliveries)=>{

    const getBowlers = deliveries.filter(eachBall=> Number(eachBall.is_super_over) > 0).reduce((acc,eachBall)=> {
        if(acc[eachBall.bowler]){
            acc[eachBall.bowler]["balls"] +=1;
            acc[eachBall.bowler]["runs"] += Number(eachBall.total_runs);
            acc[eachBall.bowler]["eco"] = acc[eachBall.bowler].runs/(acc[eachBall.bowler].balls/6) ;
        }else{
            acc[eachBall.bowler] = {};
            acc[eachBall.bowler]["balls"] = 1;
            acc[eachBall.bowler]["runs"] = Number(eachBall.total_runs);
        }
        return acc;
    },{});

    const getBowlersEconomy = Object.entries(getBowlers).reduce((acc,curr)=>{
         acc[curr[0]] = curr[1].eco;
        return acc;
    },{});
    
    const result = Object.entries(getBowlersEconomy).sort(([,a],[,b])=>[,a][1]-[,b][1])[0];

    console.log(result);
}


//...........................................................................//


const main = async ()=>{
    await csv()
        .fromFile(MATCHES_FILE_PATH)
        .then(matches=>{
            csv()
            .fromFile(DELIVERIES_FILE_PATH)
            .then(deliveries=>{
                getStrikeRate(matches,deliveries);
                getHighestDismissal(deliveries);
                getBestEcoInSuperOver(deliveries);
            })
        })
};

main();
